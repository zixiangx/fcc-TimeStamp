This is an implementation of a Timestamp Microservice API that converts Unix 
Epoch time to Natural Language time and vice versa. It accounts for parsing 
errors and returns null if input is invalid.

It is built on the .NET Core platform.

The working demo can be found at the following link.
http://zx-timestampmicroservice.azurewebsites.net