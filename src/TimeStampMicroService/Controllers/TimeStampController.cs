﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TimeStampMicroService.Interfaces;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace TimeStamp.Controllers
{
    [Route("/")]
    public class TimeStampController : Controller
    {
        protected readonly ITimeStampService _timeStampService;
        public TimeStampController(ITimeStampService timeStampService)
        {
            _timeStampService = timeStampService;
        }

        [HttpGet("{input}")]
        public JsonResult Get(string input)
        {
            // Constants
            const string FAVICON = "favicon";
            Regex regex = new Regex("[a-zA-Z]");

            // Variables
            int? unixTimestamp = null;
            string naturalDateTime = null;
            
            // Simplistic way of blocking Favicon requests from being processed for now.
            if (!input.Contains(FAVICON))
            {
                naturalDateTime = _timeStampService.MapAnyToNaturalDateTime(input);

                if (regex.IsMatch(input))
                {
                    // Contains alphabets, may be Natural Language DateTime
                    unixTimestamp = _timeStampService.MapNaturalLangToUnixTimestamp(input);
                }
                else
                {
                    // No alphabets, may be Unix Epoch Timestamp
                    unixTimestamp = (naturalDateTime != null) ? (int?) int.Parse(input) : null;
                };
            }
            
            return Json(new { unix = unixTimestamp, natural = naturalDateTime });
        }

        [HttpGet("/")]
        public IActionResult Get()
        {
            return View("Index");
        }
    }
}
