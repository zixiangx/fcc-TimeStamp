﻿using System;

namespace TimeStampMicroService.Interfaces
{
    public interface ITimeStampService
    {
        string MapAnyToNaturalDateTime(string input);
        int? MapNaturalLangToUnixTimestamp(string input);
    }
}