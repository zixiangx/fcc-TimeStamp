﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TimeStampMicroService.Interfaces;

namespace TimeStampMicroService.Models
{
    public class TimeStampService : ITimeStampService
    {
        public string MapAnyToNaturalDateTime(string input)
        {
            // Constants
            Regex regex = new Regex("[a-zA-Z]");
            const string dateTimeFormat = "MMMM dd, yyyy";

            // Variables
            DateTime dateTimeResult;
            string result = null;

            if (regex.IsMatch(input))
            {
                if (DateTime.TryParse(input, out dateTimeResult))
                    result = dateTimeResult.Date.ToString(dateTimeFormat);
            }
            else
            {
                dateTimeResult = new DateTime(1970, 1, 1, 0, 0, 0).AddSeconds(Convert.ToDouble(input));
                result = dateTimeResult.Date.ToString(dateTimeFormat);
            }
            
            return result;
        }

        public int? MapNaturalLangToUnixTimestamp(string input)
        {
            int? intResult = null;
            double? epochDbl = null; 
            DateTime result;

            if (DateTime.TryParse(input, out result))
                epochDbl = result.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            if (epochDbl != null)
                intResult = int.Parse(epochDbl.ToString());

            return intResult;
        }
    }
}
